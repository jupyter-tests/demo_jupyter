# jupybind-test project

Two notebooks that are using ipywidgets and can be used with voilà. It uses poetry. The `server_image_roller.ipynb` does use images from another gitlab repository that is added in the project as git submodule. This could be a convenient way to upload data.

## Binder

To deploy the project on a binder like [mybinder](https://mybinder.org/) or [jupybind](https://jupyterhub-sam.inria.fr:8002/services/binder/) we added a binder repository with two files:
* `requirements.txt` specifies the python modules we want to install globally. Basically we just need poetry.
* `postBuild` contains instructions that will be run from the shell after the environment has been installed. 
In our case we run the poetry install command to install our project dependencies. We also init and update the git submodule to get the images.

We also have to add a `poetry.toml` to tell poetry not to create an new virtual environment but to use the one that is 
deployed by binder. If we do not include this file, the command "poetry install" will not be installed in the virtual
environment of binder. But as we do not want to have this file activated elsewhere we create it in the 
`postBuild`file just before calling the `poetry install` command.

### Results on mybider:

Home page:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fjupyter-tests%2Fdemo_jupyter.git/main)

### Results on jupybind:

* Home page:

[![Binder](https://jupyterhub-sam.inria.fr:8002/services/binder/badge_logo.svg)](https://jupyterhub-sam.inria.fr:8002/services/binder/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fjupyter-tests%2Fdemo_jupyter.git/HEAD)


To get the voila renders of the local or server images rollers, when the home page is openned, replace the `tree` at the end of the url by `voila` and click the notebook you want to see.
